<?php
session_start();
include 'database.php';
?>

<!DOCTYPE html>
<html>
     <head>
	       <title>ChatsAPP</title>
	       <link rel="icon" href="image1.png" type="image/gif" sizes="16x16">
		   <link rel="stylesheet" href="style.css" media="all"/>
		   <meta charset="utf-8">
           <meta name="viewport" content="width=device-width, initial-scale=1">
	 </head>
	 
	 
	 <style>
	@media only screen and (max-width: 1000px) {
    #header,#container2,#container3 {width:80%;}}
	</style>
	
<body style="background-image: url(bgimage.jpeg);">

<h1 id="header" style="color:green; margin-top:180px;"> ChatsAPP </h1>
<div id="container3">
            <?php 
				if($_GET)
				{					
					echo '<b><label style="text-weight:bold; font-size:10pt; color:red;">'.$_GET['msg'].'</label></b>';
				}
			?>
</div>

<div id="container2">
	<form method="POST" action="logincheck.php" align="center">
		<input type="text" name="mno" class="text1" placeholder="Enter Your Mobile Number" pattern="[7-9][0-9]{9}" required title="Please Enter Only Digits or A valid Mobile Number." maxlength="10"/ >
		<br/><br/>
		<input  type="submit" name="submit" class="submit1" style="font-size:15pt;" value="Login"/>
		<br/><br/>
		<br/>
        <p align="center">New to<span style="color:green;"> ChatsAPP </span>?<b><a href="signup.php" style="text-decoration: none; color:blue">SignUp</a></b></p>
	</form>		
</div>

</body>
</html>