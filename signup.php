<?php 
include 'database.php';
?>

<!DOCTYPE html>
<html>
    <head>
	       <title>ChatsAPP</title>
	       <link rel="icon" href="image1.png" type="image/gif" sizes="16x16">
		   <link rel="stylesheet" href="style.css" media="all"/>
		   <meta charset="utf-8">
           <meta name="viewport" content="width=device-width, initial-scale=1">

<style>
	@media only screen and (max-width: 1000px) {
    #header,#container2,#container3 {width:80%;}}
	</style>		   
	</head>
	
<body style="background-image: url(bgimage.jpeg);">
<h1 id="header" style="color:green; margin-top:180px;"> ChatsAPP </h1>
<div id="container3" >
            <?php 
				if($_GET){
					if($_GET['msg']=="User Register Successfully")
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:green;">'.$_GET['msg'].'</label>';
						echo '<br>';
						echo"<a style='font-weight: bold; text-align:center; font-size:10pt; text-decoration: none;' href='index.php'>Please Login</a>";
					}
					else
					{
						echo '<label style="font-weight: bold; text-align:center; font-size:10pt; color:red;">'.$_GET['msg'].'</label>';
			
					}	
				}				   
			?>
</div>
				
<div id="container2"> 
	<form method="POST" align="center" action="signupcheck.php" enctype="multipart/form-data">
		<input type="text" name="name" class="text1" placeholder="Enter Your Name" pattern="[A-Za-z]+" id="msg" required title="!Please Enter Only Alphabets"/><br/><br/>
		<input type="text" name="mno" class="text1" placeholder="Enter Your Phone Number" pattern="[7-9][0-9]{9}" maxlength="10" required title="Please Enter Only Digits/Enter a valid Mobile number" /><br/><br/>
		<input type="submit" name="submit" class="submit1" style="font-size:15pt;" value="SignUp"/>
		<br/><br/>	
		<br/>
		<p align="center"><span style="color:green;"> ChatsAPP </span>User ?<b><a href="index.php" style="text-decoration: none; color:blue">Login</a></b></p>
	</form>					
</div>

</body>
<script>
window.onload = function() {
  document.getElementById("msg").focus();
};
</script>
</html>